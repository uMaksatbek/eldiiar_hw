﻿using MVCProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCProduct.Controllers
{
    public class ProductsController : Controller
    {
        //
        // GET: /Products/
        public ActionResult Index()
        {
            return View(DB.DbProduct.Product);
        }

        //
        // GET: /Products/Details/5
        public ActionResult Details(int id)    //
        {
            return View();
        }

        //
        // GET: /Products/Create
        public ActionResult Create()     
        {
            return View();
        }

        //
        // POST: /Products/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {
            DB.DbProduct.Product.Add(product);
            return RedirectToAction("Index");
        }

        //
        // GET: /Products/Edit/5
        public ActionResult Edit(int id)
        {
            var product = DB.DbProduct.Product
                .FirstOrDefault(p => p.ProductID == id);
            if (product == null)
                return HttpNotFound();
            return View(product);
        }

        //
        // POST: /Products/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Products/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
