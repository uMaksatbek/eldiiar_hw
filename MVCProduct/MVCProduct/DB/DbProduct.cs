﻿using MVCProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProduct.DB
{
    public static class DbProduct
    {
        public static List<Product> Product { get; set; }

        static DbProduct() 
        {
            Product = new List<Product>();
        }
    }
    
}