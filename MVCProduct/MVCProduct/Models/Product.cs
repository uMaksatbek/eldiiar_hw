﻿using MVCProduct.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCProduct.Models
{
    public class Product
    {
        public int ProductID { get; set; }
       
        [Required(ErrorMessage = "Пожалуйста введите наименование продукта")]
        [StringLength(14, ErrorMessage = "Не более 10 символов") ]       
        [Display(Name = "Наименование продукта")]
        public string ProductName { get; set; }
        public ProductType ProductType { get; set; }
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Display(Name="Дата изготовления")]
        [DataType(DataType.Date)]
        public DateTime IssueData { get; set; }
    }
}