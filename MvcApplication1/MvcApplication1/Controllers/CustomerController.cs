﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/

        public ActionResult Index()
        {
            return View();
        }
        public ViewResult ShowView() 
        {
            return View();
        }

        public JsonResult ShowJSON() 
        {
            return Json(new { id = 15 }, JsonRequestBehavior.AllowGet);
        }
        public ContentResult ShowContent() 
        {
            return Content("<h3>Hello world</h3>");
        }
        enum Month 
        {
            Январь = 01,
            Февраль = 02,
            Март = 03,
            Апрель = 04,
            Май = 05,
            Июнь = 06,
            Июль = 07, 
            Август = 08, 
            Сентябрь = 09, 
            Октябрь = 10,
            Ноябрь = 11, 
            Декабрь = 12
        }

       public bool Iss(string str)
       {
           foreach (char c in str) 
           {
               if (c < '0' || c > '9')
                   return false;
           }
           return true;
       } 
        public string GetINN(string inn) 
        {
             if (inn == null) return " ";
        
            var day = inn.Substring(0, 2);
            var month = inn.Substring(2, 2);
            var year = inn.Substring(4, 4);

            var birthDate = new DateTime(int.Parse(year), int.Parse(month), int.Parse(day));
        
            return "Здравствуйте, дата вашего рождения: " + birthDate.ToLongDateString();
     
            }
        }
    }

