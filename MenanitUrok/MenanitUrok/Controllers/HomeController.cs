﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MenanitUrok.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCurrentTime() 
        {
            var Time = DateTime.Now.ToLongTimeString();
            ViewBag.time = Time;
            return View();
        }

        public ActionResult SumNumbersGet(int a, int b) 
        {
            int sum = 0;
            sum = a + b;
            return View(sum);
        }
        public ActionResult SumNumbersPost() 
        {
            return View();
        }
        [HttpPost]
        public ActionResult SumNumbersPost(int a, int b) 
        {
            int sum = 0;
            sum = a + b;
            return View(sum);
        }

        public ActionResult GetForm()
        {
            return View();
        }


    }
}