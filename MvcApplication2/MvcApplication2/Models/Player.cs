﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Player
    {
        public int PlayerID { get; set; }
 
        public DateTime BirthDate { get; set; }

        public string Name { get; set; }
    }
}