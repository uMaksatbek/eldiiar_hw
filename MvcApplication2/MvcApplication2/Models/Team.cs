﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Team
    {
        public int TeamID { get; set; }
        public string Name { get; set; }
        public List<Player> Player { get; set; }

        public Team() 
        {
           
            Player = new List<Player>();
        }
    }
}