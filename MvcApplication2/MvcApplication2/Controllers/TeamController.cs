﻿using MvcApplication2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication2.Controllers
{
    public class TeamController : Controller
    {

        public static Team Team  = new Team
            {
            TeamID = 1,
            Name="Real Madrid"
        };

        public ActionResult Index()
        {
      
          return View(Team);
        }

        [HttpPost]
        public ActionResult AddPlayer(int PlayerID, string Name, DateTime BirthDate) 
        {
            Team.Player.Add(new Player { PlayerID = PlayerID, Name = Name, BirthDate = BirthDate });
            
            
            
            return RedirectToAction("Index");
        }
        
    }
}
