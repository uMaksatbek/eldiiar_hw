﻿using Customer.DataAccessLayer;
using Customer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customer.Controllers
{
    public class ClientController : Controller
    {
        //
        // GET: /Client/
        public ActionResult Index()
        {
            using(var context=new ClientContext())
            {
            var clients=context.Clients.ToList();
            
            return View(clients);
        
            }
        }

        //
        // GET: /Client/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Client/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Client/Create
        [HttpPost]
        public ActionResult Create(Client client)
        {
             using(var context=new ClientContext())
             {
                 context.Clients.Add(client);
                 context.SaveChanges();

                 return RedirectToAction("Index");
             }
        }

        //
        // GET: /Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Client/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
