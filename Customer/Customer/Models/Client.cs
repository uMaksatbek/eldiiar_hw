﻿using Customer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Customer.Models
{
      //   [Table("Clients")]
    public class Client
    {
        public int ClientID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
       
    }
}