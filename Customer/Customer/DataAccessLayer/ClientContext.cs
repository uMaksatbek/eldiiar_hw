﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Customer.Models;

namespace Customer.DataAccessLayer
{
    public class ClientContext:DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public ClientContext() : base("ClientsConnection") 
        {
        
        }
    }
}